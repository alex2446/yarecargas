export class Empresa
{
	id:number;
	empresa:string;
	logo:string;
	logo64:string;
	estatus:number;

	constructor()
	{
		this.id = 0;
		this.empresa = "";
		this.logo = "";
		this.logo64 = "";
		this.estatus = 0;
	}

}
