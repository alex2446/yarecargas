export class HistoriaVenta
{
	id: number;
	nombre: string;
	empresa: string;
	logo: string;
	servicio: string;
	numero: string;
	importe: string;
	fecha: string;
	hora: string;

	constructor()
	{
		this.id = 0;
		this.nombre = "";
		this.empresa = "";
		this.logo = "";
		this.servicio = "";
		this.numero = "";
		this.importe = "";
		this.fecha = "";
		this.hora = "";
	}

}