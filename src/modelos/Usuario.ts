export class Usuario
{
	id:number;
	id_rol: number;
	nombre:string;
	paterno:string;
	materno:string;
	usuario:string;
	password:string;
	correo:string;
	estatus: number;
	reset_pass: number;

	constructor()
	{
		this.id = 0;
		this.id_rol = 0;
		this.nombre = "";
		this.paterno = "";
		this.materno = "";
		this.usuario = "";
		this.password = "";
		this.correo = "";
		this.estatus = 0;
		this.reset_pass = 0;
	}

}