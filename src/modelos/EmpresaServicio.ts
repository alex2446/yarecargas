export class EmpresaServicio
{
	id_empresa_servicio:number;
	id_empresa:number;
	empresa:string;
	logo:string;
	id_servicio:number;
	servicio:string;
	estatus:number;

	constructor()
	{
		this.id_empresa_servicio = 0;
		this.id_empresa = 0;
		this.empresa = "";
		this.logo = "";
		this.id_servicio = 0;
		this.servicio="";
		this.estatus = 0;
	}

}
