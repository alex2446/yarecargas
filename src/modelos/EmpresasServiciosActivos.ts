import { Servicio } from './Servicio';
export class EmpresasServiciosActivos
{
	id_empresa:number;
	empresa:string;
	logo:string;
	servicio: Servicio[];


	constructor()
	{
		this.id_empresa = 0;
		this.empresa = "";
		this.logo = "";
		this.servicio = [];
	}

}