import {Http, URLSearchParams} from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

const api_url = "http://201.161.100.142:81/api_recargas/public/";

@Injectable()
export class VentaProvider {

	error:boolean;
	data_resp: any;

	constructor(private http: Http){}

	peticionPost(data:URLSearchParams, url:string)
	{
		return this.http.post(url, data).map(resp=>
		{
			this.data_resp=resp.json();
		});
	}

	registrarVenta(id_usuario: number, id_empresa_servicio: number, numero: string, importe: string)
	{
		let data = new URLSearchParams();
		data.append("id_usuario", id_usuario.toString());
		data.append("id_empresa_servicio", id_empresa_servicio.toString());
		data.append("numero", numero);
		data.append("importe", importe);
		let url = api_url + "venta";
		return this.peticionPost(data,url);
	}

	historicoVentas(id_usuario: number, id_rol: number)
	{
		let data = new URLSearchParams();
		data.append("id_usuario", id_usuario.toString());
		data.append("id_rol", id_rol.toString());
		let url = api_url + "ventasTotales";
		return this.peticionPost(data,url);
	}

}
