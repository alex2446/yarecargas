import { AlertController } from "ionic-angular";
import { Injectable } from '@angular/core';

@Injectable()
export class MensajeProvider 
{

	constructor(public alertCtrl: AlertController) {}

	mensaje(mensaje:string, titulo:string)
	{
		this.alertCtrl.create({
			title: titulo,
			subTitle: mensaje,
			buttons: ["OK"]
		}).present();
	}

}
