import {Http, URLSearchParams} from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Empresa } from '../../modelos/Empresa';

//const api_url = "http://devconsulting.com.mx/alex/api_recargas/public/";
const api_url = "http://201.161.100.142:81/api_recargas/public/";
//const api_url = "http://localhost/api_recargas/public/";

@Injectable()
export class EmpresaProvider 
{

	error:boolean;
	data_resp: any;

	constructor(private http: Http) {}

	peticionPost(data:URLSearchParams, url:string)
	{
		return this.http.post(url, data).map(resp=>
		{
			this.data_resp=resp.json();
		});
	}

	peticionPostJson(url:string, json:string)
	{
		return this.http.post(url, json)
		.map(resp=>{
			this.data_resp=resp.json();
		});
	}


	consultarEmpresas()
	{
		let data = new URLSearchParams();
		let url = api_url + "consultarEmpresas";
		return this.peticionPost(data,url);
	}

	activarEmpresa(id_empresa:number)
	{
		let data = new URLSearchParams();
		data.append("id_empresa", id_empresa.toString());
		let url = api_url + "activarEmpresa";
		return this.peticionPost(data,url);
	}

	desactivarEmpresa(id_empresa:number)
	{
		let data = new URLSearchParams();
		data.append("id_empresa", id_empresa.toString());
		let url = api_url + "desactivarEmpresa";
		return this.peticionPost(data,url);
	}

	altaEmpresa(empresa:Empresa)
	{
		let url = api_url + "altaEmpresa";
		console.log(JSON.stringify(empresa));
		return this.peticionPostJson(url,JSON.stringify(empresa));
	}

	actualizarEmpresa(empresa:Empresa)
	{
		let url = api_url + "actualizarEmpresa";
		return this.peticionPostJson(url,JSON.stringify(empresa));
	}

}
