import {Http, URLSearchParams} from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { EmpresaServicio } from '../../modelos/EmpresaServicio';

//const api_url = "http://devconsulting.com.mx/alex/api_recargas/public/";
const api_url = "http://201.161.100.142:81/api_recargas/public/";
//const api_url = "http://localhost/api_recargas/public/";

@Injectable()
export class EmpresaServicioCrudProvider {

	error:boolean;
	data_resp: any;

	constructor(private http: Http) {}

	peticionPost(data:URLSearchParams, url:string)
	{
		return this.http.post(url, data).map(resp=>
		{
			this.data_resp=resp.json();
		});
	}

	peticionPostJson(url:string, json:string)
	{
		return this.http.post(url, json)
		.map(resp=>{
			this.data_resp=resp.json();
		});
	}


	consultarEmpresasServicios()
	{
		let data = new URLSearchParams();
		let url = api_url + "consultarEmpresasServicios";
		return this.peticionPost(data,url);
	}

	activarEmpresaServicio(id_empresa_servicio:number)
	{
		let data = new URLSearchParams();
		data.append("id_empresa_servicio", id_empresa_servicio.toString());
		let url = api_url + "activarEmpresaServicio";
		return this.peticionPost(data,url);
	}

	desactivarEmpresaServicio(id_empresa_servicio:number)
	{
		let data = new URLSearchParams();
		data.append("id_empresa_servicio", id_empresa_servicio.toString());
		let url = api_url + "desactivarEmpresaServicio";
		return this.peticionPost(data,url);
	}

	altaEmpresaServicio(id_empresa:number, id_servicio:number)
	{
		let data = new URLSearchParams();
		data.append("id_empresa", id_empresa.toString());
		data.append("id_servicio", id_servicio.toString());		
		let url = api_url + "altaEmpresaServicio";
		return this.peticionPost(data,url);
	}

	consultarServiciosAsignables(id_empresa:number)
	{
		let data = new URLSearchParams();
		data.append("id_empresa", id_empresa.toString());
		let url = api_url + "consultarServiciosAsignables";
		return this.peticionPost(data,url);
	}

}
