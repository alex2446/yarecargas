import {Http, URLSearchParams, RequestOptions, Headers} from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Usuario } from '../../modelos/Usuario';

//const api_url = "http://devconsulting.com.mx/alex/api_recargas/public/";
const api_url = "http://201.161.100.142:81/api_recargas/public/";
//const api_url = "http://localhost/api_recargas/public/";

@Injectable()
export class UsuarioProvider {

	error:boolean;
	data_resp: any;

	constructor(private http: Http) {}

	peticionPost(data:URLSearchParams, url:string)
	{
		return this.http.post(url, data).map(resp=>
		{
			this.data_resp=resp.json();
		});
	}

	peticionPostJson(url:string, json:string)
	{
		return this.http.post(url, json)
		.map(resp=>{
			this.data_resp=resp.json();
		});
	}

	iniciarSesion(usuario: string, password: string)
	{
		let data = new URLSearchParams();
		data.append("usuario", usuario);
		data.append("password", password);
		let url = api_url + "iniciarSesion";
		return this.peticionPost(data,url);
	}

	recuperarPass(usuario: string, correo: string)
	{
		let data = new URLSearchParams();
		data.append("usuario", usuario);
		data.append("correo", correo);
		let url = api_url + "restaurarPassword";
		return this.peticionPost(data,url);
	}

	cambiarPass(id_usuario: number, pass:string)
	{
		let data = new URLSearchParams();
		data.append("id_usuario", id_usuario.toString());
		data.append("pass", pass);
		let url = api_url + "cambiarPass";
		return this.peticionPost(data,url);
	}

	consultarUsuarios()
	{
		let data = new URLSearchParams();
		let url = api_url + "consultarUsuarios";
		return this.peticionPost(data,url);
	}

	actvarUsuario(id_usuario:number)
	{
		let data = new URLSearchParams();
		data.append("id_usuario", id_usuario.toString());
		let url = api_url + "activarUsuario";
		return this.peticionPost(data,url);
	}

	desactivarUsuario(id_usuario:number)
	{
		let data = new URLSearchParams();
		data.append("id_usuario", id_usuario.toString());
		let url = api_url + "desactivarUsuario";
		return this.peticionPost(data,url);
	}

	consultarCatalogoRoles()
	{
		let data = new URLSearchParams();
		let url = api_url + "consultarRoles";
		return this.peticionPost(data,url);
	}


	altaUsuario(usuario:Usuario)
	{
		let url = api_url + "altaUsuario";
		return this.peticionPostJson(url,JSON.stringify(usuario));
	}

	actualizarUsuario(usuario:Usuario)
	{
		let url = api_url + "actualizarUsuario";
		return this.peticionPostJson(url,JSON.stringify(usuario));
	}

	webService()
	{
		let data = {'data':{"pass":"password","user":"Luis"}};
		let headers = new Headers({ 'SO': 'Android', 'Version':'2.5.2' });
		let options = new RequestOptions({ headers: headers });
		let url = "https://agentemovil.pagatodo.com/AgenteMovil.svc/agMov/login";
		return this.http.post(url, JSON.stringify(data),options)
		.map(resp=>{
			this.data_resp=resp.json();
		});
	}
}
