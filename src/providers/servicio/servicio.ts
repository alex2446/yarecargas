import {Http, URLSearchParams} from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { ServicioCrud } from '../../modelos/ServicioCrud';
//const api_url = "http://devconsulting.com.mx/alex/api_recargas/public/";
const api_url = "http://201.161.100.142:81/api_recargas/public/";
//const api_url = "http://localhost/api_recargas/public/";

@Injectable()
export class ServicioProvider 
{

  	error:boolean;
	data_resp: any;

	constructor(private http: Http) {}

	peticionPost(data:URLSearchParams, url:string)
	{
		return this.http.post(url, data).map(resp=>
		{
			this.data_resp=resp.json();
		});
	}

	peticionPostJson(url:string, json:string)
	{
		return this.http.post(url, json)
		.map(resp=>{
			this.data_resp=resp.json();
		});
	}


	consultarServicios()
	{
		let data = new URLSearchParams();
		let url = api_url + "consultarServicios";
		return this.peticionPost(data,url);
	}

	activarServicio(id_servicio:number)
	{
		let data = new URLSearchParams();
		data.append("id_servicio", id_servicio.toString());
		let url = api_url + "activarServicio";
		return this.peticionPost(data,url);
	}

	desactivarServicio(id_servicio:number)
	{
		let data = new URLSearchParams();
		data.append("id_servicio", id_servicio.toString());
		let url = api_url + "desactivarServicio";
		return this.peticionPost(data,url);
	}

	altaServicio(servicio:ServicioCrud)
	{		
		let data = new URLSearchParams();
		data.append("nombreServicio", servicio.servicio);
		let url = api_url + "altaServicio";
		return this.peticionPost(data,url);
	}

	actualizarServicio(servicio:ServicioCrud)
	{	
		let url = api_url + "actualizarServicio";
		return this.peticionPostJson(url,JSON.stringify(servicio));
	}

}
