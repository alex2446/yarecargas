import { Injectable } from '@angular/core';
import {Http, URLSearchParams} from '@angular/http';

const api_url = "http://201.161.100.142:81/api_recargas/public/";
//const api_url = "http://devconsulting.com.mx/alex/api_recargas/public/";

@Injectable()
export class EmpresaServicioProvider {

	error:boolean;
	data_resp: any;

	constructor(private http: Http) {}

	peticionPost(data:URLSearchParams, url:string)
	{
		return this.http.post(url, data).map(resp=>
		{
			this.data_resp=resp.json();
		});
	}

	consultarEmpresasServiciosActivos()
	{
		let data = new URLSearchParams();
		let url = api_url + "consultarEmpresaServiciosActivos";
		return this.peticionPost(data,url);
	}



}