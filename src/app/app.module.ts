import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { IonicStorageModule } from '@ionic/storage';
import { HttpModule } from '@angular/http';

/*#################################################################################*/
/*                                PAGES                                            */
/*#################################################################################*/
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { RecuperarPassPage } from '../pages/recuperar-pass/recuperar-pass';
import { ServiciosEmpresaPage } from '../pages/servicios-empresa/servicios-empresa';
import { CambiarPassPage } from '../pages/cambiar-pass/cambiar-pass';
import { VentaPage } from '../pages/venta/venta';
import { DetalleVentaPage } from '../pages/detalle-venta/detalle-venta';
import { ListarEmpresasPage } from '../pages/empresas/listar-empresas/listar-empresas';
import { FormularioEmpresaPage } from '../pages/empresas/formulario-empresa/formulario-empresa';
import { ListarUsuariosPage } from '../pages/usuarios/listar-usuarios/listar-usuarios';
import { FormularioUsuarioPage } from '../pages/usuarios/formulario-usuario/formulario-usuario';
import { ListarServiciosPage } from '../pages/servicios/listar-servicios/listar-servicios';
import { FormularioServicioPage } from '../pages/servicios/formulario-servicio/formulario-servicio';
import { ListarEmpresaServicioPage } from '../pages/empresa-servicio/listar-empresa-servicio/listar-empresa-servicio';
import { AgregarEmpresaServicioPage } from '../pages/empresa-servicio/agregar-empresa-servicio/agregar-empresa-servicio';

/*#################################################################################*/
/*                                PROVIDERS                                        */
/*#################################################################################*/
import { MensajeProvider } from '../providers/mensaje/mensaje';
import { UsuarioProvider } from '../providers/usuario/usuario';
import { EmpresaServicioProvider } from '../providers/empresa-servicio/empresa-servicio';
import { VentaProvider } from '../providers/venta/venta';
import { EmpresaProvider } from '../providers/empresa/empresa';
import { Camera } from '@ionic-native/camera';
import { ServicioProvider } from '../providers/servicio/servicio';
import { EmpresaServicioCrudProvider } from '../providers/empresa-servicio-crud/empresa-servicio-crud';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    RecuperarPassPage,
    CambiarPassPage,
    ServiciosEmpresaPage,
    VentaPage,
    DetalleVentaPage,
    ListarEmpresasPage,
    ListarUsuariosPage,
    ListarServiciosPage,
    AgregarEmpresaServicioPage,
    ListarEmpresaServicioPage,
    FormularioUsuarioPage,
    FormularioEmpresaPage,
    FormularioServicioPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    RecuperarPassPage,
    CambiarPassPage,
    ServiciosEmpresaPage,
    VentaPage,
    DetalleVentaPage,
    ListarEmpresasPage,
    ListarUsuariosPage,
    ListarServiciosPage,
    ListarEmpresaServicioPage,
    AgregarEmpresaServicioPage,
    FormularioUsuarioPage,
    FormularioEmpresaPage,
    FormularioServicioPage
  ],
  providers: [
    StatusBar,
    Camera,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UsuarioProvider,
    MensajeProvider,
    EmpresaServicioProvider,
    VentaProvider,
    EmpresaProvider,
    ServicioProvider,
    EmpresaServicioProvider,
    EmpresaServicioCrudProvider,
    EmpresaServicioCrudProvider
  ]
})
export class AppModule {}
