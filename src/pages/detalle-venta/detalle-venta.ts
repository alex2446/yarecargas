import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';
import { VentaProvider } from '../../providers/venta/venta';
import { MensajeProvider } from '../../providers/mensaje/mensaje';

import { HomePage } from '../home/home';
import { Usuario } from '../../modelos/Usuario';

@Component({
  selector: 'page-detalle-venta',
  templateUrl: 'detalle-venta.html',
})
export class DetalleVentaPage {

	fecha: string;
	hora: string;
	logo: string;
	numero: string;
	importe: string;
	usuario: Usuario;
	id_empresa_servicio: number;

	modal_detalle: boolean;

	ticket: string;

  constructor(public navCtrl: NavController, 
  	          public navParams: NavParams,
  	          public viewCtrl: ViewController,
  	          public _venta: VentaProvider,
  	          public loadingCtrl: LoadingController,
  	          public _mensaje: MensajeProvider) 
  {
  	this.logo = this.navParams.get('logo');
  	this.numero = this.navParams.get('numero');
  	this.importe = this.navParams.get('importe');
  	this.usuario = this.navParams.get('usuario');
  	this.id_empresa_servicio = this.navParams.get('id_empresa_servicio');

  	this.fecha = new Date().toLocaleDateString();
  	this.hora = new Date().toLocaleTimeString();

  	this.modal_detalle = true;
  }

  cancelar()
  {
  	this.viewCtrl.dismiss();
  }

  aceptar()
  {
  	this.registrarVentaPromesa().then(()=>
  	{
  		this.modal_detalle = false;
  	});
  }

  finalizarRecarga()
  {
  	this.navCtrl.setRoot(HomePage,{usuario: this.usuario});
  }

  registrarVentaPromesa()
  {
  	let promesa = new Promise((resolve,reject)=>
  		{
			const loader = this.loadingCtrl.create(
            {
                content: "Procesando recarga...",
            });
            loader.present();

            this._venta.registrarVenta(this.usuario.id,this.id_empresa_servicio,this.numero, this.importe).subscribe(()=>
            	{
            		if(!this._venta.data_resp.error)
					{
						this.ticket = this._venta.data_resp.data.ticket;
						loader.dismiss();  
						resolve();
					}
					else
					{
						this._mensaje.mensaje(
							this._venta.data_resp.mensaje,		
		                    "Error");
						loader.dismiss();  
						resolve();
					}
            	},
	            (()=>
	            {
	                resolve();
	                loader.dismiss();
	                this._mensaje.mensaje(
	                	"Se ha perdido conexión con el servidor",
	                    "Error de Conexión");
	            }));
        });
  	return promesa;
  }
}
