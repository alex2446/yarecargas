import { Component } from '@angular/core';
import { NavController, LoadingController,NavParams, ModalController } from 'ionic-angular';
import { EmpresaServicioProvider } from '../../providers/empresa-servicio/empresa-servicio';
import { MensajeProvider } from '../../providers/mensaje/mensaje';
import { LoginPage } from '../login/login';
import { CambiarPassPage } from '../cambiar-pass/cambiar-pass';
import { EmpresasServiciosActivos } from '../../modelos/EmpresasServiciosActivos';

import { ServiciosEmpresaPage } from '../servicios-empresa/servicios-empresa';
import { Usuario } from '../../modelos/Usuario';
import { VentaPage } from '../../pages/venta/venta';
import { Servicio } from '../../modelos/Servicio';
import { HistoriaVenta } from '../../modelos/historiaVenta';
import { VentaProvider } from '../../providers/venta/venta';

import { ListarEmpresasPage } from '../empresas/listar-empresas/listar-empresas';
import { ListarUsuariosPage } from '../usuarios/listar-usuarios/listar-usuarios';
import { ListarServiciosPage } from '../servicios/listar-servicios/listar-servicios';
import { ListarEmpresaServicioPage } from '../empresa-servicio/listar-empresa-servicio/listar-empresa-servicio';


@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage 
{
	public categoria: string = 'recargas';
	public categorias: Array<string> = ['recargas', 'recaudacion', 'administracion'];

	usuario: Usuario;
	ventas: HistoriaVenta [];
	ventas_aux: HistoriaVenta [];

	empresasServicios: EmpresasServiciosActivos[];
	
	constructor(public navCtrl: NavController,
		        public _empresaServicio: EmpresaServicioProvider,
		        public loadingCtrl: LoadingController,
		        public _mensaje: MensajeProvider,
		        public navParams: NavParams,
		        public modalCtrl: ModalController,
		        public _venta: VentaProvider) 
	{
		this.usuario = this.navParams.get('usuario');
		this.reestablecerPassword();
	}

	reestablecerPassword()
	{
		if(this.usuario.reset_pass == 1)
		{
			let profileModal = this.modalCtrl.create(CambiarPassPage, { reset: true, id_usuario: this.usuario.id });
   			profileModal.present();
		}
	}

	cambiarPass()
	{
		let profileModal = this.modalCtrl.create(CambiarPassPage, { reset: false, id_usuario: this.usuario.id });
		profileModal.present();		
	}

	ionViewWillEnter()
	{
		if (this.usuario.reset_pass == 0)
		{
			this.consultarEmpresasServiciosActivosPromesa().then(()=>
			{});
		}
	}

	cerrarSesion()
	{
		this.navCtrl.setRoot(LoginPage);
	}

	consultarEmpresasServiciosActivosPromesa()
	{
		let promesa = new Promise((resolve,reject)=>{
			const loader = this.loadingCtrl.create(
            {
                content: "Consultando Servicios disponibles...",
            });
            loader.present();
				this._empresaServicio.consultarEmpresasServiciosActivos().subscribe(()=>
				{
					if(!this._empresaServicio.data_resp.error)
					{
						this.empresasServicios = this._empresaServicio.data_resp.data;
						loader.dismiss();  
						resolve();
					}
					else
					{
						this._mensaje.mensaje(
							this._empresaServicio.data_resp.mensaje,		
		                    "Error");
						loader.dismiss();  
						resolve();
					}
				},
	            (()=>
	            {
	                resolve();
	                loader.dismiss();
	                this._mensaje.mensaje(
	                	"Se ha perdido conexión con el servidor",
	                    "Error de Conexión");
	            }));
		});
		return promesa;
	}

	verMas(empresa:EmpresasServiciosActivos)
	{
		let profileModal = this.modalCtrl.create(ServiciosEmpresaPage, { empresa: empresa, usuario: this.usuario });
		profileModal.present();	
	}

	venta(id_empresa_servicio: number, logo: string, servicio: string)
	{
		this.navCtrl.push(VentaPage,{ id_empresa_servicio: id_empresa_servicio, 
			                          logo: logo, 
			                          usuario: this.usuario,
			                          servicio: servicio});
	}

	onTabChanged(event)
	{
		if(event.value == 'recaudacion')
		{
			this.historicoVentasPromesa().then(()=>
				{
					console.log('#####################');
					console.log(this.ventas);
				});
		}
	}

	historicoVentasPromesa()
	{
		let promesa = new Promise((resolve,reject)=>
		{
			const loader = this.loadingCtrl.create(
            {
                content: "Consultando Historico de ventas...",
            });
            loader.present();

            this._venta.historicoVentas(this.usuario.id, this.usuario.id_rol).subscribe(()=>
            	{
            		if(!this._venta.data_resp.error)
					{
						this.ventas = this._venta.data_resp.data;
						this.ventas_aux = this._venta.data_resp.data;
						loader.dismiss();  
						resolve();
					}
					else
					{
						this._mensaje.mensaje(
							this._venta.data_resp.mensaje,		
		                    "Error");
						loader.dismiss();  
						resolve();
					}
            	});
        });

        return promesa;
    }

	listarUsuarios()
	{
		this.navCtrl.push(ListarUsuariosPage);
	}

	listarEmpresas()
	{
		this.navCtrl.push(ListarEmpresasPage);
	}

	listarServicios()
	{
		this.navCtrl.push(ListarServiciosPage);
	}

	agregarEmpresasServicios()
	{
		this.navCtrl.push(ListarEmpresaServicioPage);
	}

	inicializarElementos()
	{
		this.ventas = this.ventas_aux;
	}

	getItems(ev: any)
	{
		this.inicializarElementos();

		const valor = ev.target.value;

		if (valor && this.ventas) 
		{
			this.ventas = this.ventas.filter((item) => 
			{
				return (item.id.toString().toLowerCase().indexOf(valor.toLowerCase()) > -1);
			});
		}
	}

}
