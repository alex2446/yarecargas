import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { DetalleVentaPage } from '../detalle-venta/detalle-venta';
import { MensajeProvider } from '../../providers/mensaje/mensaje';
import { Usuario } from '../../modelos/Usuario';

@Component({
  selector: 'page-venta',
  templateUrl: 'venta.html',
})
export class VentaPage {

	id_empresa_servicio: number;
	usuario: Usuario;
	logo: string;
	servicio: string;
	numero: string;
	importe: string;

	constructor(public navCtrl: NavController, 
		        public navParams: NavParams,
		        public modalCtrl: ModalController,
		        public _mensaje: MensajeProvider) 
	{
		this.id_empresa_servicio = this.navParams.get('id_empresa_servicio');
		this.usuario = this.navParams.get('usuario');
		this.logo = this.navParams.get('logo');
		this.servicio = this.navParams.get('servicio');
	}

	detalleVenta()
	{
		if (this.numero == "" && this.importe == "")
		{
			this._mensaje.mensaje("El importe y el número son requeridos", "Error");
		}
		else
		{
			let profileModal = this.modalCtrl.create(DetalleVentaPage,{logo: this.logo, 
			                                                       numero: this.numero, 
			                                                       importe: this.importe,
			                                                   	   usuario: this.usuario,
			                                                   	   id_empresa_servicio: this.id_empresa_servicio},
			                                                   	  {cssClass : 'pricebreakup'});
			profileModal.present();	
		}
	}

}
