import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { Empresa } from '../../../modelos/Empresa';
import { EmpresaProvider } from '../../../providers/empresa/empresa';
import { MensajeProvider } from '../../../providers/mensaje/mensaje';
import { FormularioEmpresaPage } from '../formulario-empresa/formulario-empresa';


@Component({
	selector: 'page-listar-empresas',
	templateUrl: 'listar-empresas.html',
})
export class ListarEmpresasPage 
{
	lista:Empresa[];
	public categoria: string = 'activas';
	public categorias: Array<string> = ['activas', 'inactivas'];

	constructor(public navCtrl: NavController, 
		public loadingCtrl: LoadingController,
		public navParams: NavParams,
		public _empresa: EmpresaProvider,
		public _mensaje: MensajeProvider) {
	}

	ionViewWillEnter()
	{
		this.consultarEmpresas();
	}

	//CONSULTAR TODOS LOS REGISTROS
	consultarEmpresas() 
	{
		this._empresa.consultarEmpresas().subscribe(()=>
		{
			if (!this._empresa.data_resp.error)
			{
				this.lista = this._empresa.data_resp.data;
			}
			else
			{
				this.lista = [];
				this._mensaje.mensaje(this._empresa.data_resp.mensaje,"EMPRESAS");  
			}
		}, (()=>{
			this._mensaje.mensaje("Error de conexion con el servidor","ERROR");
		}));      
	}

	formularioEmpresa(alta:boolean, empresa:Empresa)
	{
		this.navCtrl.push(FormularioEmpresaPage, {'alta': alta, 'empresa': empresa});
	}


	activarEmpresa(id_empresa:number)
	{
		const loader = this.loadingCtrl.create({
			content: "Activando empresa...",
		});
		loader.present();
		this._empresa.activarEmpresa(id_empresa).subscribe(()=>
		{
			if (!this._empresa.data_resp.error)
			{
				this.consultarEmpresas();
				loader.dismiss();
				this._mensaje.mensaje(this._empresa.data_resp.mensaje,"EMPRESAS");  
			}
			else
			{
				loader.dismiss();
				this._mensaje.mensaje(this._empresa.data_resp.mensaje,"EMPRESAS");  
			}
		}, (()=>
		{
			loader.dismiss();
			this._mensaje.mensaje("Error de conexion con el servidor","ERROR");

		}));  

	}	
	
	desactivarEmpresa(id_empresa:number)
	{
		const loader = this.loadingCtrl.create({
			content: "Desactivando empresa...",
		});
		loader.present();
		this._empresa.desactivarEmpresa(id_empresa).subscribe(()=>
		{
			if (!this._empresa.data_resp.error)
			{
				this.consultarEmpresas();
				loader.dismiss();
				this._mensaje.mensaje(this._empresa.data_resp.mensaje,"EMPRESAS");  
			}
			else
			{
				loader.dismiss();
				this._mensaje.mensaje(this._empresa.data_resp.mensaje,"EMPRESAS");  
			}
		}, (()=>
		{
			loader.dismiss();
			this._mensaje.mensaje("Error de conexion con el servidor","ERROR");
		}));  

	}

}
