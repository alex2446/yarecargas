import { Component } from '@angular/core';
import { NavController, NavParams,LoadingController } from 'ionic-angular';
import { Empresa } from '../../../modelos/Empresa';
import { EmpresaProvider } from '../../../providers/empresa/empresa';
import { MensajeProvider } from '../../../providers/mensaje/mensaje';
import { Camera, CameraOptions } from '@ionic-native/camera';

@Component({
	selector: 'page-formulario-empresa',
	templateUrl: 'formulario-empresa.html',
})
export class FormularioEmpresaPage 
{
	empresa:Empresa;
	alta: boolean;
	accion: String;

	constructor(public navCtrl: NavController, 
		public navParams: NavParams,
		public loadingCtrl: LoadingController,
		public _empresa: EmpresaProvider,
		public camera: Camera,
		public _mensaje: MensajeProvider) {
		this.empresa = new Empresa();
		this.alta=navParams.get('alta');
		this.accion="AGREGAR";
		if(!this.alta)
		{
			this.accion="ACTUALIZAR";
			this.empresa=navParams.get('empresa');
		}		
		//this.empresa.logo64="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEhUTEhIVFRUWFRUVFRYVFRcVFRUVFRUWFhUVFRUYHSggGBolHRUVITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OGhAQGi0dHx4tLS0tLS0tMC0tLS0vLS0wLS0tLS0tLS0tLS0uLS0tLS0tLS0tLS0tLS0tLS0tLS0rLf/AABEIAOEA4QMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAAAQIDBAUGBwj/xABCEAABAwMBBAYIAgcHBQAAAAABAAIRAwQhMRJBUWEFBnGBkaEHEyIyscHR8JLhFBUjQkNSgiQzcrKz0vE0YqKjwv/EABoBAAMBAQEBAAAAAAAAAAAAAAABAgMEBQb/xAAtEQACAgEEAQEHAwUAAAAAAAAAAQIDEQQSITFBEwUUIlFhgaEysfAVYnGRwf/aAAwDAQACEQMRAD8A8QCVIEqYwQlI8+Y8+CQBMQoCUNSgJwQJsQN5p4p8/ilaFM1MhsjFMcR5/RTttxx+P0TQFOwEowRKTEZRG8jz+imNuziPA/RMZTJ+CsU7YzlMxlLHkYKTOLfA/RKKbToR4O+iutsApqdrIganlgDOSZmcDdmeWQwdqM1trO8eDvorAtANHg79HT3+ytJlpGYnd2Z1+I7099s3adsyRPsyACRzAJjxRgyd5jutxPvDwd9FC+35jwd9FsVrTGRlVX254iDvRguNpl1aA4jwP0VR9ON/xW263G+dM6c4jyWfcU5P2PJJnTXZkz3M5qIhW6lNQPakdEWQwkTiEVHkmSSTxJk+JSNEMQhCChEJUkIAEIQgBUICVAAgIShMTHAJwCQBTMYgliNYptnTJnfPGToZziM4yT2lWMVllOFRlKRCykrVKmn06atUqMmE8HPOY2nTxMK3RpZGVJbW/E933qr9GiIJA03/ABhPBxW24IaVMGBpuEmOQydFNb2MiQc6fktG3sQIj696v07OBpuJIEmI39kDKeDzrNTjoxm2bozyzPbiO/yS29tsvkmccBjet/8ARwOM8wfngIo9HjWe/j+SeDn97+Zg31tI0xjP1VGqwYAHbwXW3dswQNBvjeI3z3LPq0YbDpwMDcAZOPGe8pYNKtTwc1cge0ABBxppmfZJ0WXWprorihJ0jeJ1Ky61NS0epTaYlWmqtRi1q1FVa1NTg9CEzLe1RuCtvaq7wkdMWRISkJEGgiRKhIYiEqVAAEiVIgGKE8BNATwmJjmhTsCiaFPTCZnImorRFIAxIdB1bMHskA+SpUmK9ToE4GFSOWxkjQNw78yVoW9OAcZOnzhRULeNFq2jBx++SpI4LrMIdStGkb8T3b+9WKVoSWt0wDP3yAVinR8t2uDy3latrTpgAkQTvcc54DkSqUTyrb2hbe0AbgCAO9aDLEQ0uI1k8MAkAclFStzs6EkkyQNBy8Fq+rPs47ZzuVqJ5NljyQ1WUy2NDBEntn2eGviSof0YCHDAOvDO8LQoWsHOYwPyHaipR9mMblW0x3szbm2a4bp3Rn4KrcWQ2QTu3bo3E9krZbbwQTrv4mNDAUF9SB2YMGdRggb9pJxNIWNHLXNlOfONeSxLuz17+xdnUoNjBnXOIPZCxr+hE4xyGf8AhQ4np6bUNPBx1VhGuOCoVmnguhu6Qznx1Cy7il97lk0e7Tbkw6zFUeFpVm6qjUapaPRgys5o8zu7Iz4+HgxSuCjISN0NSFKhBQkISwhIYhQkSpgPCnbbuLDU2fYDmsLtwc4Oc1vaQ1x7lA1OamSyZisMVZqsUkIzkXKGq06DdIKzbcZWrbNyBGv3CtHFcy3SdODmN8YO/O7ljhxWxRdLQ0uxO0Bj3oiR3YVO2YDpqO5XLejOg9oazoRuIVo8q6RctqRLgJjGJmc6ctFtWFmA4YBI0ncePksy1BgAA679THLgti1qc+fMn7KuJ5GokzWt6U9isBkJts3HNTtC2SPPayNAS7MqSEALTaGCo5mzooa1vtDPhOvarz2BV6rY3njAiefxUuIujMdR2QdkDE9gKzro7Wg7dIzx37vJa5pE4kEa40HaqdxbtacaDBM6kws2jeuXJyd5b+1mJOMCFiXzdy6fpGlqd3mezgufubcxpr4LCSPe0lnzObudSqL1pV2GT5dmR99iz6ohZM96t8FR4URVhxwdN24fHcoHBSdMWMKROKRBYiEIQA1KAhKEDFhOakStTQmSMKnpKBoVik1MzZct3Z1WxRrCM/JY9NhVwbtcaD6D71VI4rUmblvcyBI3bsFaFJxMHsPCeP3yWFZh2MRw3zyjXiugpvLWgRBJHZHCFojytQkujVt9l0OLscZPh5LQOzIIzjcN4WXZUi4zsy4xzz98V0XRvR2JfqfDwWsUeLe0i1ZvIE7sCBEA/crU0jIOBpu5dqpULFoMg+ZPxV5rFvFHGkKkKVELQ0SyMTXDz8+HxPipIQ4Sk0TtKwGICqOtg3mTnPFXajY+5VStTJ9nI5x+fJZtZJSwznOmGAhzfCM6fJc1cAwcac/uF0vSlJ7Ts+e7CwLpwEh2PsrCSPZ0nRyt072jI17fvisu5C3L+lkmQsa5blc7R9JRyii5ROCne1Qu88RwjM5ns8+GYwdiIymlOcEwhItAhIlQAQlQAlCYwATwE0KRqolkjArNFuVXYVaoowZyNK2bjVaFNpDhtT5RlZtu4RlaFmwuImYEwTqe5Wjz7fJvWlqJkYW1ZWBc+XHEciJ0iDlZVm6IBPfC37Gm4iSSJ4YIHPnqtoo8HUyaNmxtg3QDwg+KvMYfn4rNs60GBhuuh+4WtTAhbxR5Mstj2qRMY5PlaYHFCFInIhM6IwyNTHAlTBhOgJ7AVI21qHSm/wDCfopbQpaeT6TZUeSqtQ+0IMzgjnqD98Vfq2j5hzS0mInfngrH6orH+Hntb9VLlFdsIaW2baUW8fRnH9NOLdfy7de1cfc1tSdxPnP5r0Hp3oWu1rqtZrQwQ3BDiSTAJA01Xn/TLmNnAndxJOixlh8o9XS6eUHtlFpmDejkAIGM8OZPCe04AGBjViugt+jjVqUqO2A6pUa3azgvcB36rp3ei1wMG6AnhRLvP1gWarcj3ITjBcnl9RVnrS6Qt9h72fyuc0/0kj5Kg8LJo7IsruTFKQmEKDQZsoSpUgyIlCAlCaQxArvRlk+vVZSpgF7zstBMCYJ1OmiqwtLq/dCjc0arpDWVGudGTsg5gb8K0jKxtQbj3jg6a39GV+d1EdtQ/JpS9L9Qry2our1DSc1kbQpue5wacF0FgwN/LO5endVusNG7DzQLjsEB203Z94EiJ7Ct80w4FrhLSCHA6EEQQeS6lVFrg+W/q2rjZtsiljtYPnm0GFrdFAmcY3HWPsq11i6vmzuXUgD6v36Tjvpk6EnVzYIPYDvTaMsgg43jzwVko4PWtmpxzHybdlQMEuzrEdmF2nU7o413OLpbTYN2pJ0z3HyXH2tMHLvDh9SvWOrFoaFuxjh7RAc7tO7uEDuVTzGPBw0adXXfEspdlmj1dt4Ahxji4/KFk9FNY65dScJANQRP8pMc9y6uluXD3ZdSuqj2mD6xxBGT7WuO9TTKUtyz4L9pUUaf0rFBYUucLtHZN6Moj+G34/FZXTtBrCzZaGztTAjSNfFW+rdy6pSJe7aIeRJjgDu7VF1lMBna4eQ+imrcrdreTp1cabNC7IRUcpPpLyjHY0kgASThb9j0S1ol/tHhuH1UHV63Bmof8LfmfvmrfSt/6sAD3jpyHFXdZKUtkTDQaWqqn3i37fz5sukho3AeAUZuqf8AO38QXJ1nFxlxJPEpoCFpF5Zuvajk8Qj+TV6YuGl7HNcCBrBnQzuWzb3jHkhjpjXVck4rQ6tP/auEfufMJ20pQ/wLTal+u+P1vn/Rd61tm1qf0/5gvFumLBo1MnQCJI1yF7n0zSLqNQASS3AGScgxC8xvOrV0Xkst3kEbwB8YUUNKPJ0aqMvWTS8Hn3RtMtuqG7Zr0DpwqNOi9uuafHcVwTupt857X/ohBa9rh+0pD3SI1dyXo9zSJkgLeMlnhhZFtHzf1ipRc1xwr1v9RyxqgXX9cLYNvLkSB+2eY3+0dr5rlKrOfcsrI+Trh0im5ROU7mqFwXO0bIjSohCQAE4JoT2OgyNfgmimSAKZlPmo6TuQVunUxoFrGKM2z0n0LDN02d1B3+qF6lELyP0OVyK9w3+amw/hcf8Acu766dI1KNpUq0j7dM06g4HZq0y5p5ESDyJXVH9J8n7QjnWuK7lj9kO659E/pNCGiatOX0+JIHtM7HAAdoavJqV8A3EzO+ZGdF7H0ffsrUWVqZltRocOInVp4EGQeYK8669dD+prevY32KzjtYw2rq78Ql3btJyhxuR26OXHpzNTqM11e5YC32WftHmNQPdb3uI7gV64HySSIxx8yuJ9HXR5ZQD3D2qsPM6hmlMeEu/qWl136SNG1cxpAfWBpj/D++7wx/UFjNZeDurxCLaOwtnS0Eabu/M+a866x13i+rAGBLd3FjT3aruugXTQpGTmlTPiwFcL10YBe1CdHNZrp7gGsclFHFjRn7Th6mni/r/xnV9RXD1LwD/EkzuJY36Kbrl/dMJ3VB/ld9Fl+jWrNOqM4LNTOoI+S2etoHqBOge3zkfNT1qPuG3Ps5x+j/DLXQIHqGRvBPi4rA6brH17xw2R5D/nvWx1ZrA0dkGdgkdxJI+Y7lQ6z2RDvWiYgbUbiMAny8FVb23NPzkz1UHZoYOHjGfssGbtIncsmp0mxjZ1OJAOROvaoP1ySfZbiNSfku5Hk1VWS6Nt1UKx1WvQ64LQR7jsdhaVl2PQ9zdUnvY5tPMM2gfb1nO6MZgyZVXqpaVbfpGnTrNLXObUiZIcAxxkHQ+6ue2cXGUUz1tNp5qcZSPTatQNaXOIDWgkkmAABJJO4LGr9b7Bmt3R7nT8Fo9LM2qFVvGlUHiwhfOd3TaRMx+9O4eC5tPQrE8+D2pNo9hvfSJ0aD/1EiD7tOqRjsYrNzfYkb4I5g6Y7189XIB0nfuXtllcB1Ci/XapUneLGldldCi8HDq24LJ5D17uXfrC4z+8w/ipMd81zFW4dnJ8V0npLbHSFUx7zKTv/W1v/wArk3vWFksNo6aHuri/ohKlUnefFQuKc4qMrmbNsCJUkpVOR4GgpwKanBBRIE8OUTU8LRMnB3fogqf25440H+T6f5r0zrrmwuhH8F5/CNr5Lyv0UOi/aONKoPDZd8l6z1mc11pcN22jat6w94amm4DzhdtXMOT5b2kmtfFr+1/k4P0VdMEF1o84dNSjO5wH7Rg7QNoD/tdxXoHSFiytTdSqCWuiRocGQQdxBheE2Nw9jmvYdlzSHNIzBbkFey9G9Z7V9Nj3V6dNzmgljnjaYf3m7Jzrv34K0rfw4Z36qjbZvj5/c6uyaRAiOQ0HABeY9cunDcXLw0+zTGxT57M7R7zPcAul6X6527KFQUKrX1S3ZaGgyJgF+1EYEntC86pHahwjkNNNynbl5NYr4T3PqVWLrS2J30KfiGD6LnfSCyLoOz/dMPL3nCeeih6sdf7Wjb0qTxU26bdl0NECCYyXCRCyOuHWmjdVWVKQcAGBh29nJ2nHRrjiHLCEJKzOC7luq2rs6n0Yv9q49qZFM+Bf9V0/Wo/2Z54Fh/8AIfVec9WOsjLQvfsF+00AjaLczM5ndKn6e9IP6RSfQFDY2xh/rCYggyAGjOOKmdcnbuXXBNc4+7ut94ZesOnRbva6fZJhw3FpOewg5ld/a3NOszaYQ9rh266gj5L54F6Wv945mJGQQdOa0+iOslS3JfTqv2oA4tIB02TgjXXK0tp9TldkaXfQtvcT1K/6jUHuLqb30iYkCHNxwnI8UnRnUShTM1Hvqng6GtOZyG5PiuStPSpXaP2tGm48WlzMc/ez4IvPSlcEQygymTvJNQjnuA7wVn6Wo6ydsVTnKiek9K9KUbWkalVwYxogAak7mMbvPJeXdD9Yjc9LU678S4spt/kbsPAB/ETPFy5HpzpyrWJfVqmo4jG1o3k0DAHKFzrL9zHbW05pGhadmO8QVrDTKCee2W90vofUtaoCCJmWnTmCvmurV2myMRu1krPqdM1ahO3VqO4bT3H4lR291Ez9nsVUxVWec5N4RfksOIj6BeldC9PWjbW3FS6otc2jSaWuqsBBbTAggnGi8rr3eFlXNZN3Y5J1OmjbFLODoPSRfUat5t0ajajfUsG00yNoOfIkb4jxXJkoJTCVxTlueRVV+nBRXgCmuKJSQsjQRCRCQhQnBMTgmUPCcFGE8FUmIlHNS0mgZgeCgarDXLaLJZcpuVtlTf3QqFMYVnSDx05iYx3gjuW6ZjJF0OjMd6kp1y0Az3dirNdIUXreC03YIwaVvc/zaanv0jirzKrQABmcyN4PLiue9fHP7+9Uw3ZBxgodiSE68m/+miTLsDju4dnYqdS7J1AniFkvu5/PVQOuFn6g41Gy6/bkwRpmZnjKibeZOezgVkvrphqIVhaqRs/pkameyCPFL+sMQPEnKxfXJDVVq4tQNGvdyqVWqSojUUbnLOy3Jrgl9YUvrVW2km2sHMaLDqige9NLlGSocgFcU2UEpqhiAlEoQpAEJZQgBE4JqUIAcnNTQnAq0DJGqVqhapWlWmQy204U5OFUplSbS1UjNosMeRvTHPTHOj6pjnqnISQ57lC96Y56iL1m5GiRI5yYXKMuTZUbikiQuRtKMlJKNwyXbRtKKUSluGSFybKbKJS3DFTZRKaVLAUlNJSpqQAUICEhAhCRIBUJEIAVCEJgOCcFGCnymgJWlSNKgaU8K0yGTypnMLSQ4EEYI4FVWlPBxG77+ipMlomLlG5yaTzTXFGQSBxUZSkphKlstCEpChCTZSBCaiUhjpQmSlSAWUJEoKWQCEhRKSUABSJSkQIJQhIkAIQhAAhCEAAShIhACpWn7+9d6RCaAeCnAqMFKCqJJZSgqOUoKAJS9MJTZSSnkWBSU2UFCWSkCCkKRIYqISSiUhjgkSISAJRKRCBZBEpEJgKhIhIAQhCABCEIAEIQgAQhKgAQhK1xBkbsoAREpS6clInkB7ROmdT3AST4BEpiWUxCpU2USgBZQU2USgYqRIlSAEJZSJACEJEAKkSwkQAIQhAAhCVACIQhAAhCEACEIQAJUqEAIUIQgACVKhADEIQmAIQhADmpEIQIEqEJDEKEITAEIQkA5MQhA2CEIQIUpQhCBjUqEIEKmpUIGxEIQgR//9k=";

	}

	public seleccionarImagen()
	{
		const options: CameraOptions = {
			quality: 50,
			destinationType: this.camera.DestinationType.DATA_URL,
			encodingType: this.camera.EncodingType.JPEG,
			mediaType: this.camera.MediaType.PICTURE,
			sourceType: 2
		}

		this.camera.getPicture(options).then((imageData) => 
		{
			this.empresa.logo="";
			this.empresa.logo64 = 'data:image/jpeg;base64,' + imageData;
		}, 
		(err) => 
		{
			console.log("ERROR AL SELECCIONAR LA IMAGEN:", JSON.stringify(err));
		});
	}


	agregarEmpresa()
	{
		const loader = this.loadingCtrl.create({
			content: "Agregando registro...",
		});
		loader.present();

		this._empresa.altaEmpresa(this.empresa).subscribe(()=>
		{
			if (!this._empresa.data_resp.error)
			{
				loader.dismiss();
				this._mensaje.mensaje(this._empresa.data_resp.mensaje,"REGISTRO AGREGADO");
				this.navCtrl.pop();
			}
			else
			{
				loader.dismiss();
				this._mensaje.mensaje(this._empresa.data_resp.mensaje,"ERROR");
			}
		}, (()=>{
			loader.dismiss();
			this._mensaje.mensaje("Error de conexion con el servidor","Error");
		}));

	}

	actualizarEmpresa()
	{
		const loader = this.loadingCtrl.create({
			content: "Actualizar registro...",
		});
		loader.present();

		this._empresa.actualizarEmpresa(this.empresa).subscribe(()=>
		{
			if (!this._empresa.data_resp.error)
			{
				loader.dismiss();
				this._mensaje.mensaje(this._empresa.data_resp.mensaje,"REGISTRO ACTUALIZADO");
				this.navCtrl.pop();
			}
			else
			{
				loader.dismiss();
				this._mensaje.mensaje(this._empresa.data_resp.mensaje,"ERROR");
			}
		}, (()=>{
			loader.dismiss();
			this._mensaje.mensaje("Error de conexion con el servidor","Error");
		}));
	}



}
