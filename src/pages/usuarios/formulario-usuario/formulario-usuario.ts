import { Component } from '@angular/core';
import { NavController, NavParams,LoadingController } from 'ionic-angular';
import { Usuario } from '../../../modelos/Usuario';
import { Role } from '../../../modelos/Role';
import { UsuarioProvider } from '../../../providers/usuario/usuario';
import { MensajeProvider } from '../../../providers/mensaje/mensaje';

@Component({
	selector: 'page-formulario-usuario',
	templateUrl: 'formulario-usuario.html',
})

export class FormularioUsuarioPage 
{
	usuario:Usuario
	alta: boolean;
	accion: String;
	catRoles: Role[];

	constructor(public navCtrl: NavController, 
		public navParams: NavParams,
		public loadingCtrl: LoadingController,
		public _usuario: UsuarioProvider,
		public _mensaje: MensajeProvider) {
		this.usuario = new Usuario();
		this.alta=navParams.get('alta');
		this.accion="AGREGAR";
		if(!this.alta)
		{
			this.accion="ACTUALIZAR";
			this.usuario=navParams.get('usuario');
		}		
	}

	ionViewWillEnter()
	{
		this.consultarCatalogoRoles();
	}

	consultarCatalogoRoles()
	{
		this._usuario.consultarCatalogoRoles().subscribe(()=>
		{
			if (!this._usuario.data_resp.error)
			{
				this.catRoles = this._usuario.data_resp.data;
			}
		}, (()=>
		{
			this._mensaje.mensaje("Error de conexion con el servidor","Error");
		}));   
	}


	limpiarSeleccion(dato:any)
	{
		switch (dato) 
		{
			case "id_rol":
			this.usuario.id_rol = 0;
			break;
		}
	}


	agregarUsuario()
	{
		const loader = this.loadingCtrl.create({
			content: "Agregando registro...",
		});
		loader.present();

		this._usuario.altaUsuario(this.usuario).subscribe(()=>
		{
			if (!this._usuario.data_resp.error)
			{
				loader.dismiss();
				this._mensaje.mensaje(this._usuario.data_resp.mensaje,"USUARIO AGREGADO");
				this.navCtrl.pop();
			}
			else
			{
				loader.dismiss();
				this._mensaje.mensaje(this._usuario.data_resp.mensaje,"ERROR");
			}
		}, (()=>{
			loader.dismiss();
			this._mensaje.mensaje("Error de conexion con el servidor","Error");
		}));

	}

	actualizarUsuario()
	{
		const loader = this.loadingCtrl.create({
			content: "Actualizar registro...",
		});
		loader.present();

		this._usuario.actualizarUsuario(this.usuario).subscribe(()=>
		{
			if (!this._usuario.data_resp.error)
			{
				loader.dismiss();
				this._mensaje.mensaje(this._usuario.data_resp.mensaje,"USUARIO ACTUALIZADO");
				this.navCtrl.pop();
			}
			else
			{
				loader.dismiss();
				this._mensaje.mensaje(this._usuario.data_resp.mensaje,"ERROR");
			}
		}, (()=>{
			loader.dismiss();
			this._mensaje.mensaje("Error de conexion con el servidor","Error");
		}));
	}


}
