import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { Usuario } from '../../../modelos/Usuario';
import { UsuarioProvider } from '../../../providers/usuario/usuario';
import { MensajeProvider } from '../../../providers/mensaje/mensaje';
import { FormularioUsuarioPage } from '../formulario-usuario/formulario-usuario';

@Component({
	selector: 'page-listar-usuarios',
	templateUrl: 'listar-usuarios.html',
})

export class ListarUsuariosPage 
{
	usuarios:Usuario[];
	public categoria: string = 'activos';
	public categorias: Array<string> = ['activos', 'inactivos'];

	constructor(public navCtrl: NavController, 
				public loadingCtrl: LoadingController,
				public navParams: NavParams,
				public _usuario: UsuarioProvider,
				public _mensaje: MensajeProvider) {
	}

	ionViewWillEnter()
	{
		this.consultarUsuarios();
	}

	//CONSULTAR TODOS LOS REGISTROS
	consultarUsuarios() 
	{
		this._usuario.consultarUsuarios().subscribe(()=>
		{
			if (!this._usuario.data_resp.error)
			{
				this.usuarios = this._usuario.data_resp.data;
			}
			else
			{
				this.usuarios = [];
				this._mensaje.mensaje(this._usuario.data_resp.mensaje,"USUARIOS");  
			}
		}, (()=>{
			this._mensaje.mensaje("Error de conexion con el servidor","ERROR");
		}));      
	}

	formularioUsuario(alta:boolean, usuario:Usuario)
	{
		this.navCtrl.push(FormularioUsuarioPage, {'alta': alta, 'usuario': usuario});
	}

	activarusuario(id_usuario:number)
	{
		const loader = this.loadingCtrl.create({
			content: "Activando usuario...",
		});
		loader.present();
		this._usuario.actvarUsuario(id_usuario).subscribe(()=>
		{
			if (!this._usuario.data_resp.error)
			{
				this.consultarUsuarios();
				loader.dismiss();
				this._mensaje.mensaje(this._usuario.data_resp.mensaje,"USUARIOS");  
			}
			else
			{
				loader.dismiss();
				this._mensaje.mensaje(this._usuario.data_resp.mensaje,"USUARIOS");  
			}
		}, (()=>
		{
			loader.dismiss();
			this._mensaje.mensaje("Error de conexion con el servidor","ERROR");

		}));  

	}	
	
	desactivarusuario(id_usuario:number)
	{
		const loader = this.loadingCtrl.create({
			content: "Desactivando usuario...",
		});
		loader.present();
		this._usuario.desactivarUsuario(id_usuario).subscribe(()=>
		{
			if (!this._usuario.data_resp.error)
			{
				this.consultarUsuarios();
				loader.dismiss();
				this._mensaje.mensaje(this._usuario.data_resp.mensaje,"USUARIOS");  
			}
			else
			{
				loader.dismiss();
				this._mensaje.mensaje(this._usuario.data_resp.mensaje,"USUARIOS");  
			}
		}, (()=>
		{
			loader.dismiss();
			this._mensaje.mensaje("Error de conexion con el servidor","ERROR");
		}));  

	}

}
