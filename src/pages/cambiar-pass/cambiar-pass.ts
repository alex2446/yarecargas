import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';

import { MensajeProvider } from '../../providers/mensaje/mensaje';
import { UsuarioProvider } from '../../providers/usuario/usuario';
import { LoginPage } from '../../pages/login/login';

@Component({
	selector: 'page-cambiar-pass',
	templateUrl: 'cambiar-pass.html',
})
export class CambiarPassPage {

	id_usuario: number;
	reset: boolean;
	pass1: string = "";
	pass2: string = "";

	constructor(public navCtrl: NavController, 
		        public navParams: NavParams,
		        public viewCtrl: ViewController,
		        public _mensaje: MensajeProvider,
		        public _usuario: UsuarioProvider,
		        public loadingCtrl: LoadingController) 
	{
		this.id_usuario = this.navParams.get('id_usuario');
		this.reset = this.navParams.get('reset');
	}

	cerrar()
	{
		this.viewCtrl.dismiss();
	}

	cambiarPass()
	{
		if(this.pass1 == this.pass2 && this.pass1.length > 0 && this.pass2.length > 0)
		{
			this.cambiarPassPromesa().then(()=>{
				this.navCtrl.setRoot(LoginPage);
			});			
		}
		else
		{
			this._mensaje.mensaje("Las contraseñas no coinciden", "Error");
		}
		
	}

	cambiarPassPromesa()
	{
		let promesa = new Promise((resolve,reject)=>{
			const loader = this.loadingCtrl.create(
            {
                content: "Actualizando Contraseña...",
            });
            loader.present();

            this._usuario.cambiarPass(this.id_usuario, this.pass1).subscribe(()=>
            {
            	if(!this._usuario.data_resp.error)
            	{
            		this._mensaje.mensaje(this._usuario.data_resp.mensaje,"Contraseña Actualizada");
            		loader.dismiss();  
					resolve();
            	}
            	else
            	{
            		this._mensaje.mensaje(this._usuario.data_resp.mensaje,"Error");
            		loader.dismiss();  
					resolve();
            	}
            });

        });
        return promesa;
	}


}
