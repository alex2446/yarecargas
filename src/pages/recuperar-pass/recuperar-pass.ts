import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';

import { UsuarioProvider } from '../../providers/usuario/usuario';
import { MensajeProvider } from '../../providers/mensaje/mensaje';

@Component({
  selector: 'page-recuperar-pass',
  templateUrl: 'recuperar-pass.html',
})
export class RecuperarPassPage 
{
	usuario: string = "";
	correo: string = "";
	error: boolean;

	constructor(public navCtrl: NavController, 
		        public navParams: NavParams,
		        public viewCtrl: ViewController,
		        public loadingCtrl: LoadingController,
		        public _usuario: UsuarioProvider,
		        public _mensaje: MensajeProvider) {}

	cerrar()
	{
		this.viewCtrl.dismiss();
	}

	recuperarPass()
	{
		this.recupearPassPromesa().then(()=>{
			if (!this.error)
			{
				this._mensaje.mensaje(
					"Se ha enviado un correo con la nueva contraseña",
                    "Contraseña Reestablecida");
				this.viewCtrl.dismiss();
			}
		});
	}

	recupearPassPromesa()
	{
		this.error = true;
		let promesa = new Promise((resolve,reject)=>{
			const loader = this.loadingCtrl.create(
            {
                content: "Validando usuario y correo...",
            });
            loader.present();

            if (this.usuario != "" && this.correo != "")
			{
				this._usuario.recuperarPass(this.usuario,this.correo).subscribe(()=>
				{
					if(!this._usuario.data_resp.error)
					{
						this.error = false;
					}
					else
					{
						this.usuario = "";
						this.correo = "";
						this._mensaje.mensaje(
							"Usuario y/o Correo incorrectos",
		                    "Error");
					}
					loader.dismiss();  
					resolve();
				},
	            (()=>
	            {
	            	this.usuario = "";
					this.correo = "";
	                resolve();
	                loader.dismiss();
	                this._mensaje.mensaje(
	                	"Se ha perdido conexión con el servidor",
	                    "Error de Conexión");
	            }));
			}
			else
			{
				this.usuario = "";
				this.correo = "";
				resolve();
				loader.dismiss();  
				this._mensaje.mensaje(
						"Longitud de campos pequeña",
	                    "Error");
			}
		});
		return promesa;
	}
}
