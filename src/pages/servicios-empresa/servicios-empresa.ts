import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

import { EmpresasServiciosActivos } from '../../modelos/EmpresasServiciosActivos';
import { Usuario } from '../../modelos/Usuario';
import { VentaPage } from '../venta/venta';

@Component({
	selector: 'page-servicios-empresa',
	templateUrl: 'servicios-empresa.html',
})
export class ServiciosEmpresaPage {

	empresa: EmpresasServiciosActivos;
	usuario: Usuario;

	constructor(public navCtrl: NavController, 
		        public navParams: NavParams,
		        public viewCtrl: ViewController) 
	{
		this.empresa = this.navParams.get('empresa');
		this.usuario = this.navParams.get('usuario');
	}

	cerrar()
	{
		this.viewCtrl.dismiss();
	}

	venta(id_empresa_servicio: number, servicio: string)
	{
		this.navCtrl.push(VentaPage,{ id_empresa_servicio: id_empresa_servicio, 
			                          logo: this.empresa.logo, 
			                          usuario: this.usuario,
			                          servicio: servicio});
	}
}
