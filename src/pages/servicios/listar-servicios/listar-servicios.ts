import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { ServicioCrud } from '../../../modelos/ServicioCrud';
import { ServicioProvider } from '../../../providers/servicio/servicio';
import { MensajeProvider } from '../../../providers/mensaje/mensaje';
import { FormularioServicioPage } from '../formulario-servicio/formulario-servicio';


@Component({
	selector: 'page-listar-servicios',
	templateUrl: 'listar-servicios.html',
})
export class ListarServiciosPage {

	lista:ServicioCrud[];
	public categoria: string = 'activas';
	public categorias: Array<string> = ['activas', 'inactivas'];

	constructor(public navCtrl: NavController, 
		public loadingCtrl: LoadingController,
		public navParams: NavParams,
		public _servicio: ServicioProvider,
		public _mensaje: MensajeProvider) {
	}

	ionViewWillEnter()
	{
		this.consultarServicios();
	}

	//CONSULTAR TODOS LOS REGISTROS
	consultarServicios() 
	{
		this._servicio.consultarServicios().subscribe(()=>
		{
			if (!this._servicio.data_resp.error)
			{
				this.lista = this._servicio.data_resp.data;
			}
			else
			{
				this.lista = [];
				this._mensaje.mensaje(this._servicio.data_resp.mensaje,"SERVICIOS");  
			}
		}, (()=>{
			this._mensaje.mensaje("Error de conexion con el servidor","ERROR");
		}));      
	}

	formularioServicios(alta:boolean, servicio:ServicioCrud)
	{
		this.navCtrl.push(FormularioServicioPage, {'alta': alta, 'servicio': servicio});
	}


	activarServicio(id_servicio:number)
	{
		const loader = this.loadingCtrl.create({
			content: "Activando servicio...",
		});
		loader.present();
		this._servicio.activarServicio(id_servicio).subscribe(()=>
		{
			if (!this._servicio.data_resp.error)
			{
				this.consultarServicios();
				loader.dismiss();
				this._mensaje.mensaje(this._servicio.data_resp.mensaje,"SERVICIOS");  
			}
			else
			{
				loader.dismiss();
				this._mensaje.mensaje(this._servicio.data_resp.mensaje,"SERVICIOS");  
			}
		}, (()=>
		{
			loader.dismiss();
			this._mensaje.mensaje("Error de conexion con el servidor","ERROR");

		}));  

	}	
	
	desactivarServicio(id_servicio:number)
	{
		const loader = this.loadingCtrl.create({
			content: "Desactivando servicio...",
		});
		loader.present();
		this._servicio.desactivarServicio(id_servicio).subscribe(()=>
		{
			if (!this._servicio.data_resp.error)
			{
				this.consultarServicios();
				loader.dismiss();
				this._mensaje.mensaje(this._servicio.data_resp.mensaje,"SERVICIOS");  
			}
			else
			{
				loader.dismiss();
				this._mensaje.mensaje(this._servicio.data_resp.mensaje,"SERVICIOS");  
			}
		}, (()=>
		{
			loader.dismiss();
			this._mensaje.mensaje("Error de conexion con el servidor","ERROR");
		}));  

	}

}
