import { Component } from '@angular/core';
import { NavController, NavParams,LoadingController } from 'ionic-angular';
import { ServicioCrud } from '../../../modelos/ServicioCrud';
import { ServicioProvider } from '../../../providers/servicio/servicio';
import { MensajeProvider } from '../../../providers/mensaje/mensaje';


@Component({
	selector: 'page-formulario-servicio',
	templateUrl: 'formulario-servicio.html',
})
export class FormularioServicioPage {

	servicio:ServicioCrud;
	alta: boolean;
	accion: String;

	constructor(public navCtrl: NavController, 
		public navParams: NavParams,
		public loadingCtrl: LoadingController,
		public _servicio: ServicioProvider,
		public _mensaje: MensajeProvider) {
		this.servicio = new ServicioCrud();
		this.alta=navParams.get('alta');
		this.accion="AGREGAR";
		if(!this.alta)
		{
			this.accion="ACTUALIZAR";
			this.servicio=navParams.get('servicio');
		}		

	}

	agregarServicio()
	{
		const loader = this.loadingCtrl.create({
			content: "Agregando registro...",
		});
		loader.present();

		this._servicio.altaServicio(this.servicio).subscribe(()=>
		{
			if (!this._servicio.data_resp.error)
			{
				loader.dismiss();
				this._mensaje.mensaje(this._servicio.data_resp.mensaje,"REGISTRO AGREGADO");
				this.navCtrl.pop();
			}
			else
			{
				loader.dismiss();
				this._mensaje.mensaje(this._servicio.data_resp.mensaje,"ERROR");
			}
		}, (()=>{
			loader.dismiss();
			this._mensaje.mensaje("Error de conexion con el servidor","Error");
		}));

	}

	actualizarServicio()
	{
		const loader = this.loadingCtrl.create({
			content: "Actualizar registro...",
		});
		loader.present();

		this._servicio.actualizarServicio(this.servicio).subscribe(()=>
		{
			if (!this._servicio.data_resp.error)
			{
				loader.dismiss();
				this._mensaje.mensaje(this._servicio.data_resp.mensaje,"REGISTRO ACTUALIZADO");
				this.navCtrl.pop();
			}
			else
			{
				loader.dismiss();
				this._mensaje.mensaje(this._servicio.data_resp.mensaje,"ERROR");
			}
		}, (()=>{
			loader.dismiss();
			this._mensaje.mensaje("Error de conexion con el servidor","Error");
		}));
	}


}
