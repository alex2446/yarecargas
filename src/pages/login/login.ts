import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Platform, ModalController } from 'ionic-angular';
import { UsuarioProvider } from '../../providers/usuario/usuario';
import { MensajeProvider } from '../../providers/mensaje/mensaje';

import { Storage } from '@ionic/storage';

import { Usuario } from '../../modelos/Usuario';
import { HomePage } from '../home/home';
import { RecuperarPassPage } from '../recuperar-pass/recuperar-pass';

@Component({
	selector: 'page-login',
	templateUrl: 'login.html',
})

export class LoginPage {
	
	usuario: string = "";
	password: string = "";
	error: boolean;

	usuarioLogueado = Usuario;

	constructor(public navCtrl: NavController, 
		        public navParams: NavParams,
		        public _usuario: UsuarioProvider,
		        public loadingCtrl: LoadingController,
		        public plt: Platform,
		        public storage: Storage,
		        public _mensaje: MensajeProvider,
		        public modalCtrl: ModalController) {}

	login()
	{
		this.iniciarSesion().then(()=>
			{
				if(!this.error)
				{
					this.almacenarUsuario().then(()=>
						{
							this.navCtrl.setRoot(HomePage,{usuario: this.usuarioLogueado});
						});
				}
			});
	}

	iniciarSesion()
	{
		this.error = true;
		let promesa = new Promise((resolve,reject)=>{
			const loader = this.loadingCtrl.create(
            {
                content: "Validando usuario y contraseña...",
            });
            loader.present();

            if (this.usuario.length > 0 && this.password.length > 0)
			{
				this._usuario.iniciarSesion(this.usuario,this.password).subscribe(()=>
				{
					if(!this._usuario.data_resp.error)
					{
						this.usuarioLogueado = this._usuario.data_resp.data;
						this.error = false;
					}
					else
					{
						this.usuario = "";
						this.password = "";
						this._mensaje.mensaje(
		                    "Error",
		                    "Usuario y/o Contraseña incorrectos");
					}
					loader.dismiss();  
					resolve();
				},
	            (()=>
	            {
	                resolve();
	                loader.dismiss();
	                this._mensaje.mensaje(
	                    "Error de Conexión",
	                    "Se ha perdido conexión con el servidor");
	            }));
			}
			else
			{
				this.usuario = "";
				this.password = "";
				resolve();
				loader.dismiss();  
				this._mensaje.mensaje(
	                    "Error",
	                    "Longitud de campos pequeña");
			}
		});
		return promesa;
	}

	almacenarUsuario()
    {
        let promesa = new Promise((resolve,reject) => 
        {
            if (this.plt.is('cordova'))
            {
                this.storage.ready().then(()=>
                {
                    this.storage.remove('usuario');
                    this.storage.set('usuario',this.usuarioLogueado);  
                    resolve();
                });
            }
            else
            {
                localStorage.removeItem('usuario');
                localStorage.setItem('usuario',  JSON.stringify(this.usuarioLogueado)); 
                resolve();
            }                
        });
        return promesa;
    }

    recuperarPass()
    {
    	const modal = this.modalCtrl.create(RecuperarPassPage);
    	modal.present();
    }

    webService()
    {
    	this._usuario.webService().subscribe(()=>{
    		console.log(this._usuario.data_resp);
    	});
    }

}
