import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { EmpresaServicioCrudProvider } from '../../../providers/empresa-servicio-crud/empresa-servicio-crud';
import { MensajeProvider } from '../../../providers/mensaje/mensaje';
import { Empresa } from '../../../modelos/Empresa';
import { ServicioCrud } from '../../../modelos/ServicioCrud';
import { EmpresaProvider } from '../../../providers/empresa/empresa';
import { ServicioProvider } from '../../../providers/servicio/servicio';

@Component({
	selector: 'page-agregar-empresa-servicio',
	templateUrl: 'agregar-empresa-servicio.html',
})
export class AgregarEmpresaServicioPage 
{
	id_empresa:number;
	id_servicio:number;
	catEmpresas:Empresa[];
	catServicio:ServicioCrud[];

	constructor(public navCtrl: NavController, 
		public navParams: NavParams,
		public loadingCtrl: LoadingController,
		public _empresaServicio: EmpresaServicioCrudProvider,
		public _empresa: EmpresaProvider,
		public _mensaje: MensajeProvider) {
		
		this.id_empresa=0;
		this.id_servicio=0;
	}


	ionViewWillEnter()
	{
		this.consultarCatalogoEmpresas();
	}

	consultarCatalogoEmpresas()
	{
		this._empresa.consultarEmpresas().subscribe(()=>
		{
			if (!this._empresa.data_resp.error)
			{
				this.catEmpresas = this._empresa.data_resp.data;
			}
			else
			{
				this.catEmpresas = [];
				this._mensaje.mensaje(this._empresa.data_resp.mensaje,"EMPRESAS");  
			}
		}, (()=>{
			this._mensaje.mensaje("Error de conexion con el servidor","ERROR");
		}));
		
	}

	consultarCatalogoServicios(id_empresa:number)
	{
		this._empresaServicio.consultarServiciosAsignables(id_empresa).subscribe(()=>
		{
			if (!this._empresaServicio.data_resp.error)
			{
				this.catServicio = this._empresaServicio.data_resp.data;
			}
			else
			{
				this.catServicio = [];
				this._mensaje.mensaje(this._empresaServicio.data_resp.mensaje,"EMPRESAS");  
			}
		}, (()=>{
			this._mensaje.mensaje("Error de conexion con el servidor","ERROR");
		}));
		
	}


	agregarEmpresaServicio()
	{
		const loader = this.loadingCtrl.create({
			content: "Agregando registro...",
		});
		loader.present();

		this._empresaServicio.altaEmpresaServicio(this.id_empresa,this.id_servicio).subscribe(()=>
		{
			if (!this._empresaServicio.data_resp.error)
			{
				loader.dismiss();
				this._mensaje.mensaje(this._empresaServicio.data_resp.mensaje,"REGISTRO AGREGADO");
				this.navCtrl.pop();
			}
			else
			{
				loader.dismiss();
				this._mensaje.mensaje(this._empresaServicio.data_resp.mensaje,"ERROR");
			}
		}, (()=>{
			loader.dismiss();
			this._mensaje.mensaje("Error de conexion con el servidor","Error");
		}));

	}


	limpiarSeleccion(dato:any)
	{
		switch (dato) 
		{
			case "id_empresa":
			this.id_empresa = 0;
			break;
			case "id_servicio":
			this.id_servicio = 0;
			break;
		}
	}

}
