import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { EmpresaServicio } from '../../../modelos/EmpresaServicio';
import { EmpresaServicioCrudProvider } from '../../../providers/empresa-servicio-crud/empresa-servicio-crud';
import { MensajeProvider } from '../../../providers/mensaje/mensaje';
import { AgregarEmpresaServicioPage } from '../agregar-empresa-servicio/agregar-empresa-servicio';



@Component({
	selector: 'page-listar-empresa-servicio',
	templateUrl: 'listar-empresa-servicio.html',
})
export class ListarEmpresaServicioPage {

	lista:EmpresaServicio[];
	public categoria: string = 'activas';
	public categorias: Array<string> = ['activas', 'inactivas'];

	constructor(public navCtrl: NavController, 
		public loadingCtrl: LoadingController,
		public navParams: NavParams,
		public _empresa: EmpresaServicioCrudProvider,
		public _mensaje: MensajeProvider) {
	}

	ionViewWillEnter()
	{
		this.consultarEmpresasServicios();
	}

	//CONSULTAR TODOS LOS REGISTROS
	consultarEmpresasServicios() 
	{
		this._empresa.consultarEmpresasServicios().subscribe(()=>
		{
			if (!this._empresa.data_resp.error)
			{
				this.lista = this._empresa.data_resp.data;
			}
			else
			{
				this.lista = [];
				this._mensaje.mensaje(this._empresa.data_resp.mensaje,"EMPRESA-SERVICIOS");  
			}
		}, (()=>{
			this._mensaje.mensaje("Error de conexion con el servidor","ERROR");
		}));      
	}

	formularioEmpresaServicio()
	{
		this.navCtrl.push(AgregarEmpresaServicioPage);
	}


	activarEmpresaServicio(id_empresa_servicio:number)
	{
		const loader = this.loadingCtrl.create({
			content: "Activando empresa-servicio...",
		});
		loader.present();
		this._empresa.activarEmpresaServicio(id_empresa_servicio).subscribe(()=>
		{
			if (!this._empresa.data_resp.error)
			{
				this.consultarEmpresasServicios();
				loader.dismiss();
				this._mensaje.mensaje(this._empresa.data_resp.mensaje,"EMPRESA-SERVICIO");  
			}
			else
			{
				loader.dismiss();
				this._mensaje.mensaje(this._empresa.data_resp.mensaje,"EMPRESA-SERVICIO");  
			}
		}, (()=>
		{
			loader.dismiss();
			this._mensaje.mensaje("Error de conexion con el servidor","ERROR");

		}));  

	}	
	
	desactivarEmpresaServicio(id_empresa_servicio:number)
	{
		const loader = this.loadingCtrl.create({
			content: "Desactivando empresa-servicio...",
		});
		loader.present();
		this._empresa.desactivarEmpresaServicio(id_empresa_servicio).subscribe(()=>
		{
			if (!this._empresa.data_resp.error)
			{
				this.consultarEmpresasServicios();
				loader.dismiss();
				this._mensaje.mensaje(this._empresa.data_resp.mensaje,"EMPRESA-SERVICIO");  
			}
			else
			{
				loader.dismiss();
				this._mensaje.mensaje(this._empresa.data_resp.mensaje,"EMPRESA-SERVICIO");  
			}
		}, (()=>
		{
			loader.dismiss();
			this._mensaje.mensaje("Error de conexion con el servidor","ERROR");
		}));  

	}


}
